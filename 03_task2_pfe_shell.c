/*
Autor Stefan Pfeiffer is160132
Improved with knowledge from Leither Manuel
<!--       _
.__(.)< (MEOW)
\___)
~~~~~~~~~~~~~~~~~~-->


 This programm needs on the server the 032-cmdpipe created as root in /var/spool/
 sudo mkfifo /var/spool/032-cmdpipe && chmod 0777 /var/spool/032-cmdpipe


*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/wait.h>
#include <unistd.h>
#include <pwd.h> // getpwent() etc
#include <grp.h> // getgrent() etc
#include <time.h>
#include <signal.h>
#include <limits.h> // For PATH_MAX
#include <libgen.h> // basename()/dirname()
#include <sys/utsname.h> // For uname()
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#define MAXCMDLEN 10000
#define MAXCMDS 10
#define MAXARGS 1000
#define MAXLEN 4096
extern char **environ;
int bg_counter = 0;
static int bg_pids [1024];
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
char cwd[PATH_MAX];
int status;
pid_t shell_pid;
const char listen_fifo[] = "/var/spool/032-cmdpipe"; //needs to be created my root
int fifo_fd;

int clone_env();
void exithandler();

int readline(char *input, size_t size) {
  if(NULL == fgets(input, size, stdin)) {
    return 1;
  }
  // Remove the trailing newline
  int len = strlen(input)-1;
  if(input[len] == '\n' || input[len] == '\r') {
    input[len] = '\0';
  }
  return 0;
}

// Signalhandler
void sig_handler(int signo){
  waitpid(-1, &status, WNOHANG);
  bg_counter--;
}

void *npipereader(){
  for(;;){
    pthread_mutex_lock(&lock);

    pthread_mutex_unlock(&lock);
    sleep(1);
  }
}


void prompt(){
  char cwd[PATH_MAX];
  getcwd(cwd,sizeof(cwd));
  printf("032-%s ",cwd);
  fflush(stdout);
}
void getpwd(){
  char cwd[PATH_MAX];
  getcwd(cwd,sizeof(cwd));
  printf("%s \n",cwd);
}

void execArgs(char** parsed){
  pid_t pid;
  int bg_flag = 0, j;
  if(parsed[0][0] == '&'){
    bg_flag++;
    for(int i=0;i<MAXCMDS;i++){
      parsed[i-1]=parsed[i];
    }
  }
  switch (pid=fork()){
    case -1:
    perror("fork");
    break;
    case 0:
    if(bg_flag==1){
      signal(SIGINT, SIG_IGN);
      signal(SIGQUIT, SIG_IGN);
      fclose(stdin);
      fopen("/dev/null", "r");
      if(execvp(parsed[0],parsed)){
        perror("exec");
        _exit(0);
        }
        break;
      }
    }else{
      //Im Vordergrund
      signal(SIGINT, SIG_DFL);
      signal(SIGQUIT, SIG_DFL);
      if(execvp(parsed[0],parsed)){
        perror("exec");
        _exit(0);
      }
      break;
    }
    default:
    if(bg_flag==0){
      pthread_mutex_lock(&lock);
      waitpid(pid,&status,0);
      bg_counter++;
      if (WIFEXITED(status))
      printf("Child-prozess end with signal %d\n", WEXITSTATUS(status));
      else
      printf("Child-prozess terminated with signal %d\n", WTERMSIG(status));
      pthread_mutex_unlock(&lock);
    }

    else{
      printf("%s starts with PID: %d\n",parsed[0] , pid);
      for(j = 0; (bg_pids[j] != 0) && (j <= 1023); j++)
      continue;
      if(j > 1023){
        waitpid(pid, &status, 0);
        break;
      }
      bg_pids[j] = pid;
      if(bg_flag >= 1)
      bg_counter++;

    }
  }
}

void execArgsPiped(char** parsed, char** parsedpipe){
  int pipefd[2];
  pid_t p1,p2;
  if(pipe(pipefd) < 0){
    printf("\n Pipe cant be init..\n" );
    return;
  }
  p1 = fork();
  if(p1 < 0){
    printf("\n Failed to pipe\n");
    return;
  }

  if(p1 == 0){
    close(pipefd[0]);
    dup2(pipefd[1], STDOUT_FILENO);
    close(pipefd[1]);

    if(execvp(parsed[0], parsed)< 0){
      printf("\n Cant execute command 1..\n" );
      exit(0);
    }

  }else{
    p2 = fork();
    if(p2 < 0){
      printf("\nFailed to Fork\n" );
      return;
    }

    if(p2 == 0){
      close(pipefd[1]);
      dup2(pipefd[0], STDIN_FILENO);
      close(pipefd[0]);
      if (execvp(parsedpipe[0], parsedpipe) < 0) {
        printf("\nCould not execute command 2..");
        exit(0);
      }
    } else {
      // parent executing, waiting for two children

      wait(NULL);
      wait(NULL);
    }
  }
}

// function for finding pipe
int parsePipe(char* str, char** strpiped) {
  int i;
  for (i = 0; i < 2; i++) {
    strpiped[i] = strsep(&str, "|");
    if (strpiped[i] == NULL)
    break;
  }
  if (strpiped[1] == NULL)
  return 0; // returns zero if no pipe is found.
  else {
    return 1;
  }
}

// function for parsing command words
void parseSpace(char* str, char** parsed) {
  int i;
  for (i = 0; i < MAXARGS; i++) {
    parsed[i] = strsep(&str, " ");
    if (parsed[i] == NULL)
    break;
    if (strlen(parsed[i]) == 0)
    i--;
  }
}

int ownCmdHandler(char** parsed){
  char cwd[PATH_MAX], pat[PATH_MAX];
  int i , switchArg = 0, noCmds = 7;
  char* ListOfOwnCmds[noCmds];
  const char *envpath = getenv("PATH");

  ListOfOwnCmds[0] = "032-ende";
  ListOfOwnCmds[1] = "032-wo";
  ListOfOwnCmds[2] = "cd";
  ListOfOwnCmds[3] = "032-info";
  ListOfOwnCmds[4] = "032-setpath";
  ListOfOwnCmds[5] = "032-addpath";
  ListOfOwnCmds[6] = "quit";

  for(i = 0; i < noCmds; i++ ){
    if (0 == (strcmp(parsed[0], ListOfOwnCmds[i]))){
      switchArg = i + 1;
      break;
    }
  }

  if(switchArg == noCmds) switchArg = 1;

  switch (switchArg) {
    case 1:
    printf("Goodbye\n");
    exit(0);
    case 2:
    getpwd();
    return 1;
    case 3:
    //if(0 == strcmp(parsed[1], "\0"))
    //  chdir(getenv(home));
    //    else
    chdir(parsed[1]);
    return 1;
    case 4:
    printf("process env of shell(%d):\n", getpid());
    printf("PID -- UID -- EUID -- PPID -- Working Directory \n");
    printf("%d - %d - %d -- %d -- %s\n", getpid(), getuid(), geteuid(), getppid(), getcwd(cwd, sizeof(cwd)));
    int i = 0;
    while(environ[i])
    printf("%s\n", environ[i++]); // Prints in form of "variable=value"
    getpwd();
    return 1;
    case 5:
    if(parsed[1] == NULL){
      printf("PATH NOT CHANGED!\n");
      return 1;
    }else{
      setenv("PATH",parsed[1],1);
      envpath = getenv("PATH");
      printf("PATH is now : %s\n", envpath);
      return 1;
    }
    case 6:
    if(parsed[1] == NULL){
      printf("PATH NOT CHANGED!\n");
      return 1;
    }
    strcpy(pat,envpath);
    strcat(pat,":");
    strcat(pat,parsed[1]);
    setenv("PATH",pat,1);
    envpath = getenv("PATH");
    printf("PATH is now : %s\n", envpath);
    return 1;
  }

  return 0;
}

int processString(char* str, char** parsed, char** parsedpipe){
  char* strpiped[2];
  int piped = 0;
  piped = parsePipe(str, strpiped);

  if (piped) {
    parseSpace(strpiped[0], parsed);
    parseSpace(strpiped[1], parsedpipe);
  } else {
    parseSpace(str, parsed);
  }
  if (ownCmdHandler(parsed))
  return 0;
  else
  return 1 + piped;
}

void handler(const char *fifo_p){
  printf("%d read FIFO %s\n",getpid(),fifo_p);
  int fifo_fd = open(fifo_p, O_RDONLY);
  if(fifo_fd < 0){
    fprintf(stderr,"client Fifo error %s: %s\n",fifo_p,strerror(fifo_fd));
    return;
  }
  if(dup2(fifo_fd, STDIN_FILENO) < 0){
    fprintf(stderr,"client Fifo error\n");
    exit(1);
  }
  if(clone_env()){
    fprintf(stderr,"Cant clone env\n");
    exit(1);
  }
  // Read command
  char input[MAXCMDLEN], *parsedArgs[MAXCMDS];
  char* parsedArgsPiped[MAXCMDS];
  if(readline(input, MAXCMDLEN)) {
    fprintf(stderr, "Could not read command from line...\n");
    exit(1); // This should close open FDs automatically
  }

  if(!(*input == '\0' || input[0] == ' ')){
    int cmd_flag = processString(input,parsedArgs,parsedArgsPiped);
    if(cmd_flag == 1)
    execArgs(parsedArgs);
    if(cmd_flag == 2)
    execArgsPiped(parsedArgs, parsedArgsPiped);
  }

}

int clone_env() {
  char stderr_fifo_path[PATH_MAX], stdout_fifo_path[PATH_MAX], remote_cwd[PATH_MAX];
  uid_t uid;
  gid_t gid;
  mode_t remote_umask;

  // UID
  if(fread(&uid, sizeof(uid_t), 1, stdin) < 1) {
    fprintf(stderr,"Could not read UID from remote\n");
    return 1;
  }
  if(setuid(uid)) {
    fprintf(stderr, "Could not set UID to %d\n", uid);
    return 1;
  }

  // GID
  if(fread(&gid, sizeof(gid_t), 1, stdin) < 1) {
    fprintf(stderr, "Could not read GID from remote\n");
    return 1;
  }
  if(setgid(gid)) {
    fprintf(stderr, "Could not set GID to %d\n", gid);
  }

  // umask
  if(fread(&remote_umask, sizeof(mode_t), 1, stdin) < 1) {
    fprintf(stderr, "Could not read umask from remote\n");
    return 1;
  }
  umask(remote_umask);


  // stdout FIFO
  if(readline(stdout_fifo_path, PATH_MAX)) {
    fprintf(stderr, "Could not read stdout FIFO path from remote\n");
    return 1;
  }
  int stdout_fifo_fd = open(stdout_fifo_path, O_WRONLY);
  if(stdout_fifo_fd < 0 || dup2(stdout_fifo_fd, STDOUT_FILENO) < 0) {
    perror("Could not open/redirect stdout FIFO");
  }
  fprintf(stderr, "[%d] Redirected stdout to %s\n", getpid(), stdout_fifo_path);

  // stderr FIFO
  if(readline(stderr_fifo_path, PATH_MAX)) {
    fprintf(stderr, "Could not read stderr FIFO path from remote\n");
    return 1;
  }
  fprintf(stderr, "[%d] Will now redirect stderr to %s\n", getpid(), stderr_fifo_path);
  int stderr_fifo_fd = open(stderr_fifo_path, O_WRONLY);
  if(stderr_fifo_fd < 0 || dup2(stderr_fifo_fd, STDERR_FILENO) < 0) {
    perror("Could not open/redirect stderr FIFO");
  }
  // Change directory
  if(readline(remote_cwd, PATH_MAX)) {
    fprintf(stderr, "Could not read working directory\n");
    return 1;
  }
  if(chdir((const char *)remote_cwd)) {
    perror("Could not change working directory");
    return 1;
  }

  char buf[PATH_MAX-1];
  while(!readline(buf, PATH_MAX-1)) {
    if(buf[0] == '\0') {
      return 0;
    }
    if(putenv(strdup(buf))) {
      perror("Could not set environment variable");
      return 1;
    }
  }
  return 1;
}



void shell(){
  char line[PATH_MAX-1];
  int readb;
  for(;;){
    printf("Waiting for client at %s \n", listen_fifo);

    fifo_fd = open(listen_fifo, O_RDONLY);
    if(fifo_fd < 0){
      fprintf(stderr, "cant open FIFO %s: %s\n",listen_fifo,strerror(fifo_fd));
      return;
    }
    if((readb = read(fifo_fd, line, PATH_MAX-1)) > 0){
      printf("Connect on %s\n", line);
      close(fifo_fd);
      pid_t handlerth = fork();
      if(handlerth < 0){
        perror("Cant fork");
        return;
      }else if(!handlerth){
        shell_pid = getpid();
        handler(line);
        return;
      }else if (readb < 0){
        perror("reading error");
        return;
      }else{
        close(fifo_fd);
      }
    }

  }
}
void exithandler() {
  close(fifo_fd);
}

int main(){
  printf("Only this and nothing more...\n");
  shell_pid = getpid();
  atexit(exithandler);
  shell();
  return 0;
}
