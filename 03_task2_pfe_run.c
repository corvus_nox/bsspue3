/*
(Copy from Leithner Manuel)
Made it worse by Stefan Pfeiffer is160132
<!--       _
.__(.)< (MEOW)
\___)
~~~~~~~~~~~~~~~~~~-->

This programm needs on the server the 032-cmdpipe created as root in /var/spool/
sudo mkfifo /var/spool/032-cmdpipe && chmod 0777 /var/spool/032-cmdpipe

*/


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/wait.h>
#include <unistd.h>
#include <pwd.h> // getpwent() etc
#include <grp.h> // getgrent() etc
#include <time.h>
#include <signal.h>
#include <limits.h> // For PATH_MAX
#include <libgen.h> // basename()/dirname()
#include <sys/utsname.h> // For uname()
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

extern char **environ;

typedef struct fifo_reader_arg {
  int fileno;
  char *path;
} fifo_reader_arg_t;

void exithandler();
int init_fifos();
int connect();
int sender();
int start_reader_thread(pthread_t *thread, fifo_reader_arg_t *reader_arg, const char *path, int fileno);
char *arg_concat(int argc, const char **argv) ;
void *stdin_writer(void *arg);
void *fifo_reader(void *arg);



const char listen_fifo[] = "/var/spool/032-cmdpipe"; //need to be made by root
const char sender_fifo_path_basename[] = "send";
const char stdout_fifo_path_basename[] = "stdout";
const char stderr_fifo_path_basename[] = "stderr";

char *tmpdir, *sender_fifo_path, *stdout_fifo_path, *stderr_fifo_path, *cmd;
int server_stdin;

int main(int argc, const char **argv) {
  tmpdir = sender_fifo_path = stdout_fifo_path = stderr_fifo_path = NULL;
  if(argc < 2) {
    printf("Please specify a command to execute as the first argument.\n");
    printf("%s will pass it to a shell server listening on %s.\n", argv[0], listen_fifo);
    return 1;
  }

  arg_concat(argc, argv);

  atexit(exithandler);

  if(init_fifos()) {
    return 1;
  }

  fprintf(stderr, "> Using sender FIFO at %s, stdout FIFO at %s and stderr FIFO at %s\n", sender_fifo_path, stdout_fifo_path, stderr_fifo_path);
  if(connect()) {
    return 1;
  }

  pthread_t stderr_reader, stdout_reader;
  fifo_reader_arg_t stderr_reader_arg, stdout_reader_arg;
  if(start_reader_thread(&stdout_reader, &stdout_reader_arg, (const char *)stdout_fifo_path, STDOUT_FILENO)) {
    fprintf(stderr, "Could not start stdout reader thread...\n");
    return 1;
  }
  if(start_reader_thread(&stderr_reader, &stderr_reader_arg, (const char *)stderr_fifo_path, STDERR_FILENO)) {
    fprintf(stderr, "Could not start stderr reader thread...\n");
    return 1;
  }

  sender();

  pthread_t stdin_writer_thread;
  if(pthread_create(&stdin_writer_thread, NULL, stdin_writer, NULL)) {
    fprintf(stderr, "Could not start stdin writer thread...\n");
    return 1;
  }


  if(pthread_join(stdout_reader, NULL) || pthread_join(stderr_reader, NULL)) {
    fprintf(stderr, "Could not join one of the reader threads, aborting...\n");
    return 1;
  }

  close(STDIN_FILENO);
  close(server_stdin);

  exit(0);
}
int connect() {
  int server_fd;
  if((server_fd = open(listen_fifo, O_WRONLY)) < 0) {
    perror("Could not open connection to server");
    return 1;
  }
  fprintf(stderr, "> Connected to server, transmitting sender FIFO path...\n");
  fflush(stdout);
  if(strlen(sender_fifo_path)+1 > write(server_fd, sender_fifo_path, strlen(sender_fifo_path)+1)) {
    fprintf(stderr, "Could not write to server, possible cause: %s\n", strerror(errno));
    close(server_fd);
    return 1;
  }
  close(server_fd);
  return 0;
}

int start_reader_thread(pthread_t *thread, fifo_reader_arg_t *reader_arg, const char *path, int fileno) {
  reader_arg->fileno = fileno;
  reader_arg->path = path;
  return pthread_create(thread, NULL, fifo_reader, (void *)reader_arg);
}


int sender() {
  fprintf(stderr, "> Waiting for connect back from server...\n");
  FILE *server_fd;
  if(NULL == (server_fd = fopen(sender_fifo_path, "w"))) {
    perror("Could not open connection to server");
    return 1;
  }
  fprintf(stderr, "> Got connection from server, transmitting FIFOs, environment and command...\n");
  fflush(stderr);

  uid_t uid = getuid();
  gid_t gid = getgid();
  mode_t mymask = umask(0);
  umask(mymask);
  char cwd[PATH_MAX];
  if(NULL == getcwd(cwd, PATH_MAX)) {
    fprintf(stderr, "Could not get current working directory, transmitting / instead...");
    strcpy(cwd, "/");
  }



  // UID
  if(fwrite(&uid, sizeof(uid_t), 1, server_fd) < 1) {
    fprintf(stderr, "Could not write UID to %s\n", sender_fifo_path);
    fclose(server_fd);
    return 1;
  }
  // GID
  if(fwrite(&gid, sizeof(gid_t), 1, server_fd) < 1) {
    fprintf(stderr, "Could not write GID to %s\n", sender_fifo_path);
    fclose(server_fd);
    return 1;
  }
  // umask
  if(fwrite(&mymask, sizeof(mode_t), 1, server_fd) < 1) {
    fprintf(stderr, "Could not write umask to %s\n", sender_fifo_path);
    fclose(server_fd);
    return 1;
  }
  fflush(server_fd);

  if(fprintf(server_fd, "%s\n%s\n", stdout_fifo_path, stderr_fifo_path) < 0) {
    fprintf(stderr, "Could not write stdout or stderr FIFO to %s\n", sender_fifo_path);
    fclose(server_fd);
    return 1;
  }
  if(fprintf(server_fd, "%s\n", cwd) < 0) {
    fprintf(stderr, "Could not write current working directory to %s\n", sender_fifo_path);
    fclose(server_fd);
    return 1;
  }
  fflush(server_fd);

  char **env = environ;
  while(*env != NULL) {
    fprintf(server_fd, "%s\n", *env);
    env++;
  }
  fflush(server_fd);

  fprintf(server_fd, "\n%s\n", cmd);
  fflush(server_fd);

  server_stdin = fileno(server_fd);
}

int init_fifos() {
  tmpdir = strdup("/tmp/032-run-XXXXXX");
  if(NULL == mkdtemp(tmpdir)) {
    perror("Could not create temporary directory");
    return 1;
  }
  // chmod 0755 the directory
  if(chmod(tmpdir, S_IRWXU | S_IXGRP | S_IRGRP | S_IXOTH | S_IROTH)) {
    perror("Could not change permissions of temporary directory");
    return 1;
  }

  tmpdir = realloc(tmpdir, sizeof(char) * PATH_MAX);
  stderr_fifo_path = calloc(sizeof(char), PATH_MAX);
  stdout_fifo_path = calloc(sizeof(char), PATH_MAX);
  sender_fifo_path = calloc(sizeof(char), PATH_MAX);
  if(NULL == tmpdir || NULL == stderr_fifo_path || NULL == stdout_fifo_path || NULL == sender_fifo_path) {
    fprintf(stderr, "Can not allocate space for FIFO path names, we're probably out of memory...\n");
    return 1;
  }
  int tmpdirlen = strlen(tmpdir);
  tmpdir[tmpdirlen] = '/', tmpdir[tmpdirlen+1] = '\0';
  strcpy(stderr_fifo_path, (const char *)tmpdir);
  strcpy(stdout_fifo_path, (const char *)tmpdir);
  strcpy(sender_fifo_path, (const char *)tmpdir);
  strncat(sender_fifo_path, sender_fifo_path_basename, PATH_MAX-tmpdirlen-2-strlen(sender_fifo_path_basename));
  strncat(stdout_fifo_path, stdout_fifo_path_basename, PATH_MAX-tmpdirlen-2-strlen(stdout_fifo_path_basename));
  strncat(stderr_fifo_path, stderr_fifo_path_basename, PATH_MAX-tmpdirlen-2-strlen(stderr_fifo_path_basename));


  mode_t old_umask = umask(0);
  if( mkfifo((const char *)stderr_fifo_path, S_IRWXU | S_IRWXG | S_IRWXO) ||
  mkfifo((const char *)stdout_fifo_path, S_IRWXU | S_IRWXG | S_IRWXO) ||
  mkfifo((const char *)sender_fifo_path, S_IRWXU | S_IRWXG | S_IRWXO)) {
    perror("Could not create FIFO");
    return 1;
  }
  umask(old_umask); // Restore old umask
  return 0;
}

void *fifo_reader(void *arg) {
  fifo_reader_arg_t my_args = *((fifo_reader_arg_t *)arg);
  int input_fd = open(my_args.path, O_RDONLY);
  char packet[PIPE_BUF];

  if(input_fd < 0) {
    fprintf(stderr, "Could not open file %s as FIFO\n", my_args.path);
    return NULL;
  }

  int bytes_read = 0;
  do {
    bytes_read = read(input_fd, packet, PIPE_BUF);
    if(bytes_read < 0) {
      fprintf(stderr, "Could not read from stream %d\n", input_fd);
      close(input_fd);
      return NULL;
    }
    if(!bytes_read) {
      break;
    }

    int bytes_written = 0;
    while(bytes_written < bytes_read) {
      int now_written = write(my_args.fileno, packet+bytes_written, bytes_read-bytes_written);
      if(now_written < 0) { // Error
        fprintf(stderr, "Could not write %d bytes to stream %d\n", bytes_read-bytes_written, my_args.fileno);
        close(input_fd);
        return NULL;
      }
      fsync(my_args.fileno);
      bytes_written += now_written;
    }
  } while(bytes_read > 0);

  fprintf(stderr, "> %s closed\n", my_args.path);
  fflush(stderr);
  return 0;
}

void *stdin_writer(void *arg) {
  char buf[PIPE_BUF];
  int bytes_read = 0, bytes_written = 0;
  while((bytes_read = read(STDIN_FILENO, &buf, PIPE_BUF)) > 0) {
    bytes_written = write(server_stdin, &buf, bytes_read);
    if(bytes_written <= 0) { // Error or EOF
      break;
    }
  }
  close(STDIN_FILENO);
  close(server_stdin);
}


void exithandler() {
  fprintf(stderr, "Cleaning up temporary files  ...\n");
  if(NULL != stderr_fifo_path) { unlink(stderr_fifo_path); free(stderr_fifo_path); }
  if(NULL != stdout_fifo_path) { unlink(stdout_fifo_path); free(stdout_fifo_path); }
  if(NULL != sender_fifo_path) { unlink(sender_fifo_path); free(sender_fifo_path); }
  if(NULL != tmpdir) { rmdir(tmpdir); free(tmpdir); }
  free(cmd);
}

char *arg_concat(int argc, const char **argv) {
  int result_len = argc;
  for(int i = 1; i < argc; i++) {
    result_len += strlen(argv[i]);
  }
  cmd = calloc(sizeof(char), result_len);
  if(NULL == cmd) {
    fprintf(stderr, "Could not allocate string buffer of length %d to store command line\n", result_len);
    exit(1);
  }
  strcpy(cmd, argv[1]);
  for(int i=2; i < argc; i++) {
    strncat(cmd, " ", 1);
    strncat(cmd, argv[i], result_len-strlen(cmd));
  }
  return cmd;
}
