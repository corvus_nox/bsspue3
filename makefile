all: clean  uebung3_1 uebung3_2_shell uebung3_2_run

uebung3_1: 03_task1_pfe.o rawio.o
	gcc -Wall 03_task1_pfe.o rawio.o -o uebung3_1 -lpthread -lcurses

uebung3_2_shell: 03_task2_pfe_shell.o
		gcc -Wall 03_task2_pfe_shell.o -o uebung3_2_shell -lpthread -lcurses

uebung3_2_run: 03_task2_pfe_run.o
				gcc -Wall 03_task2_pfe_run.o -o uebung3_2_run -lpthread -lcurses

03_task1_pfe.o: 03_task1_pfe.c
	gcc -Wall -c 03_task1_pfe.c

03_task2_pfe_shell.o: 03_task2_pfe_shell.c
		gcc -Wall -c 03_task2_pfe_shell.c

03_task2_pfe_run.o: 03_task2_pfe_run.c
		gcc -Wall -c 03_task2_pfe_run.c

rawio.o: rawio.c rawio.h
			gcc -Wall -c rawio.c -o rawio.o

clean:
	rm -f *.o uebung3_1 uebung3_2_shell uebung3_2_run rawio
