/*
Autor Stefan Pfeiffer is160132
<!--       _
       .__(.)< (MEOW)
        \___)
 ~~~~~~~~~~~~~~~~~~-->

*/
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <wait.h>
#include <signal.h>
#include "rawio.h"
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <stdarg.h>

#define MAXCMDLEN 10000
#define MAXLEN 4096
#define MAXCMDS 20
#define MAXWORDS 200
extern char **environ;
int bg_counter = 0;
static int bg_pids [1024];
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
char cwd[PATH_MAX];
	int status;
	pthread_t *uhr;

void raw_printf(int col, int row, const char *format, ...){
	char str[1024];
	va_list args;
	va_start(args, format);
	vsnprintf(str, 1024, format, args);
	va_end(args);
	writestr_raw(str, col, row);
}

void prompt(){
  getcwd(cwd,sizeof(cwd));
  raw_printf(0,get_lines()-1,"032-%s$  ",cwd);

}
void getpwd(){
  char cwd[PATH_MAX];
  getcwd(cwd,sizeof(cwd));
  raw_printf(0,get_lines()-1,"%s \n",cwd);
}

// Signalhandler
void sig_handler(int signo){
		waitpid(-1, &status, WNOHANG);
		bg_counter--;
}

void *thc(int line){
	for(;;){
		pthread_mutex_lock(&lock);
		time_t t = time(NULL);
		struct tm* tmp = gmtime(&t);

	    raw_printf(raw_get_width()-38, 0, "    032 - BG_PROCESSES: %d - %02d:%02d:%02d ", bg_counter, (tmp->tm_hour+2)%24, (tmp->tm_min), (tmp->tm_sec));
		pthread_mutex_unlock(&lock);
		sleep(1);
	}
}

void execArgs(char** parsed){
  pid_t pid;
  int bg_flag = 0, j;
    if(parsed[0][0] == '&'){
      bg_flag++;
      for(int i=0;i<MAXCMDS;i++){
      parsed[i-1]=parsed[i];
      }
    }
    switch (pid=fork()){
          case -1:
              perror("fork");
                      break;
          case 0:
            if(bg_flag==1){
              signal(SIGINT, SIG_IGN);
              signal(SIGQUIT, SIG_IGN);
              fclose(stdin);
              fopen("/dev/null", "r");
							if(execvp(parsed[0],parsed)){
								perror("exec");
								_exit(0);
							}
              break;
            }else{
              signal(SIGINT, SIG_DFL);
              signal(SIGQUIT, SIG_DFL);
              if(execvp(parsed[0],parsed)){
								perror("exec");
								_exit(0);
							}
              break;
            }
          default:
            if(bg_flag==0){
              pthread_mutex_lock(&lock);
              waitpid(pid,&status,0);
              bg_counter++;
              if (WIFEXITED(status))
                            raw_printf(0, get_lines()-1, "Child-prozess end with signal %d\n", WEXITSTATUS(status));
                        else
                            raw_printf(0, get_lines()-1, "Child-prozess terminated with signal %d\n", WTERMSIG(status));
                pthread_mutex_unlock(&lock);
            }
            else{
              raw_printf(0, get_lines()-1, "%s starts with PID: %d\n",parsed[0] , parsed);
              for(j = 0; (bg_pids[j] != 0) && (j <= 1023); j++){
                continue;
							}
                if(j > 1023){
                  waitpid(pid, &status, 0);
                  break;
                }
                bg_pids[j] = pid;
                if(bg_flag >= 1)
                  bg_counter++;
              }
            }
  }

//TODO DONT WORK
void execArgsPiped(char** parsed, char** parsedpipe){
  int pipefd[2];
  pid_t p1,p2;
  if(pipe(pipefd) < 0){
    printf("\n Pipe cant be init..\n" );
    return;
  }
  p1 = fork();
  if(p1 < 0){
    printf("\n Failed to pipe\n");
    return;
  }

  if(p1 == 0){
    close(pipefd[0]);
    dup2(pipefd[1], STDOUT_FILENO);
    close(pipefd[1]);

    if(execvp(parsed[0], parsed)< 0){
      printf("\n Cant execute command 1..\n" );
      exit(0);
    }

    }else{
      p2 = fork();
      if(p2 < 0){
        printf("\nFailed to Fork\n" );
        return;
      }

      if(p2 == 0){
        close(pipefd[1]);
        dup2(pipefd[0], STDIN_FILENO);
        close(pipefd[0]);
        if (execvp(parsedpipe[0], parsedpipe) < 0) {
          printf("\nCould not execute command 2..");
          exit(0);
        }
      } else {
        // parent executing, waiting for two children

        wait(NULL);
        wait(NULL);
      }
    }
}

// function for finding pipe
int parsePipe(char* str, char** strpiped) {
	int i;
	for (i = 0; i < 2; i++) {
		strpiped[i] = strsep(&str, "|");
		if (strpiped[i] == NULL)
			break;
	}
	if (strpiped[1] == NULL)
		return 0; // returns zero if no pipe is found.
	else {
		return 1;
	}
}

// function for parsing command words
void parseSpace(char* str, char** parsed) {
	int i;
	for (i = 0; i < MAXCMDS; i++) {
		parsed[i] = strsep(&str, " ");
		if (parsed[i] == NULL)
			break;
		if (strlen(parsed[i]) == 0)
      i--;
	}
}

int ownCmdHandler(char** parsed){
  char cwd[PATH_MAX], pat[PATH_MAX];
  int i , switchArg = 0, noCmds = 9;
  char* ListOfOwnCmds[noCmds];
  const char *envpath = getenv("PATH");

  ListOfOwnCmds[0] = "032-ende";
  ListOfOwnCmds[1] = "032-wo";
  ListOfOwnCmds[2] = "cd";
  ListOfOwnCmds[3] = "032-info";
  ListOfOwnCmds[4] = "032-setpath";
  ListOfOwnCmds[5] = "032-addpath";
  ListOfOwnCmds[6] = "032-write";
  ListOfOwnCmds[7] = "kill";
  ListOfOwnCmds[8] = "quit";

  for(i = 0; i < noCmds; i++ ){
    if (0 == (strcmp(parsed[0], ListOfOwnCmds[i]))){
      switchArg = i + 1;
      break;
    }
  }

  if(switchArg == noCmds) switchArg = 1;

  switch (switchArg) {
    case 1:
      raw_printf(0,get_lines()-1,"Goodbye\n");
			pthread_cancel(uhr);
			free(uhr);
      exit(0);
    case 2:
        getpwd();
        return 1;
    case 3:
      if(0 == strcmp(parsed[1], "\0"))
          chdir(getenv("HOME"));
          else
          chdir(parsed[1]);
		    return 1;
    case 4:
      raw_printf(0,get_lines()-3,"process env of shell(%d):\n", getpid());
      raw_printf(0, get_lines()-2,"PID -- UID -- EUID -- PPID -- Working Directory \n");
      raw_printf(0,get_lines()-1,"%d - %d - %d -- %d -- %s\n", getpid(), getuid(), geteuid(), getppid(), getcwd(cwd, sizeof(cwd)));
      int i = 0;
      while(environ[i])
        raw_printf(0,get_lines(),"%s\n", environ[i++]); // Prints in form of "variable=value"
      getpwd();
      return 1;
    case 5:
      if(parsed[1] == NULL){
        raw_printf(0,get_lines()-1,"PATH NOT CHANGED!\n");
        return 1;
      }else{
        setenv("PATH",parsed[1],1);
        envpath = getenv("PATH");
        raw_printf(0,get_lines()-1,"PATH is now : %s\n", envpath);
        return 1;
      }
      case 6:
        if(parsed[1] == NULL){
        raw_printf(0,get_lines()-1,"PATH NOT CHANGED!\n");
        return 1;
        }
        strcpy(pat,envpath);
        strcat(pat,":");
        strcat(pat,parsed[1]);
        setenv("PATH",pat,1);
        envpath = getenv("PATH");
        raw_printf(0,get_lines()-1,"PATH is now : %s\n", envpath);
        return 1;
      case 7:
      raw_printf(0, get_lines()-1, "Type: ");
      if(parsed[1][0] == 'b'){
        char string[MAXWORDS];
        gets_raw(string, MAXWORDS, strlen(cwd)+7, get_lines()-1);
				raw_printf(0,get_lines()-1, "\n");
        raw_printf(0, get_lines()-1, "\e[1m%s\e[0m\n", string);
        return 1;
      }
      else if(parsed[1][0] == 'i'){
        char string[MAXWORDS];
        gets_raw(string, MAXWORDS, strlen(cwd)+7, get_lines()-1);
				raw_printf(0,get_lines()-1, "\n");
        raw_printf(0, get_lines()-1, "\e[3m%s\e[0m\n", string);
        return 1;
      }
    case 8:
      bg_counter--;
			return 1;
  }

  return 0;
}

int processString(char* str, char** parsed, char** parsedpipe) {
	char* strpiped[2];
	int piped = 0;
	piped = parsePipe(str, strpiped);

	if (piped) {
		parseSpace(strpiped[0], parsed);
		parseSpace(strpiped[1], parsedpipe);
	} else {
		parseSpace(str, parsed);
	}
	if (ownCmdHandler(parsed))
		return 0;
	else
		return 1 + piped;
}



void shell(){
  char line[MAXCMDLEN], *parsedArgs[MAXCMDS];
  char* parsedArgsPiped[MAXCMDS];
  int cmd_flag;

	uhr=malloc(sizeof(pthread_t));
	pthread_create(uhr, NULL, (void *)thc, &clock);

  for(;;){
    signal(SIGINT, SIG_IGN);
    signal(SIGQUIT, SIG_IGN);
    signal(SIGCHLD, sig_handler);
    cmd_flag = 0;
    prompt();
    gets_raw(line,MAXCMDLEN,strlen(cwd)+6, get_lines()-1);
    raw_printf(0,get_lines()-1, "\n");
    //if(*line) line[strlen(line)-1] = '\0';
    if(!(*line == '\0' || line[0] == ' ')){
      cmd_flag = processString(line,parsedArgs,parsedArgsPiped);
      if(cmd_flag == 1)
        execArgs(parsedArgs);
      if(cmd_flag == 2)
			raw_printf(0,get_lines()-1,"Pipes are not Supported anymore!\n");
        //execArgsPiped(parsedArgs, parsedArgsPiped);
    }
  }
}

int main(){
  clearscr();
  raw_printf(0, 0, "Only this and nothing more...");
	raw_printf(0, 1, "'032-write b' for bold.");
	raw_printf(0, 2, "'032-write i' for invers.");
  shell();
  return 0;
}
